require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const corsOptions = {
  origin: "https://instaloginclone.netlify.app",
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

const port = process.env.PORT || 5000;

mongoose
  .connect(process.env.MONGOURI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  })
  .then(console.log("DB CONNECTED..."))
  .catch((err) => console.error(err));

const User = require("./userModel");

mongoose.set("strictQuery", true);

app.post("/user-login", async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = new User({
      email,
      password,
    });

    const newUser = await user.save();

    res.status(200).send(newUser);
  } catch (error) {
    res.status(500).send("something went wrong while adding the user.");
  }
});

app.listen(port, () => {
  console.log(`🚀 Server is running on port: ${port}... 🚀`);
});
